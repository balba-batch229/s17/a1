/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printWelcomeMessage(){
	let Name = prompt("What is your Name?");
	let age = prompt("How old are you?");
	let loc = prompt("Where do you live?");

	console.log("Hello, " + " " + Name + "!");
	console.log("You are" + " " + age + " " + "years old");
	console.log("You live in " + loc);
}

printWelcomeMessage();

function showSampleAlert(){
	alert("Thank you for your Input!")
}

showSampleAlert();

console.log("1. Lamb of God");
console.log("2. Metallica");
console.log("3. Slipknot");
console.log("4. Behemoth");
console.log("5. Gorgoroth");
console.log("1. The GodFather");
console.log("Rotten Tomatoes Rating 97%");
console.log("2. The GodFather 2");
console.log(" Rotten Tomatoes Rating 96%");
console.log("5. Shawshank Redemption");
console.log(" Rotten Tomatoes Rating 91%");
console.log("5. Wolf at Wall Street");
console.log(" Rotten Tomatoes Rating 93%");
console.log("5. Psycho");
console.log(" Rotten Tomatoes Rating 96%");







function friendsss(){
	let first = prompt("Enter your first friend's name.");
	let second = prompt("Enter your second friend's name.");
	let third = prompt("Enter your third friend's name.");

	console.log("You are frinds with:")
	console.log(first);
	console.log(second);
	console.log(third);
}

friendsss();

